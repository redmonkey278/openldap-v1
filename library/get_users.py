#!/usr/bin/python

import os
import json
from collections import OrderedDict
import pprint

from ansible.module_utils.basic import *

def main():
    module = AnsibleModule(
        argument_spec=dict()
    )

    scriptDir = os.path.dirname(os.path.abspath(__file__))
    userFile = open('users.json', 'r')
    users = json.load(userFile)
    #users = {"apple":1, "orange":2, "banana":3}
 
    module.exit_json(users=users, changed=False)

if __name__ == '__main__':
    main()
